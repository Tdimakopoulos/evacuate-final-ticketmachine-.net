﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicketServerTCPIP
{
	public partial class Form1 : Form
	{
		//Declare and Initialize the IP Adress
		static IPAddress ipAd = IPAddress.Parse("192.168.0.204");

		//Declare and Initilize the Port Number;
		static int PortNumber = 8888;

		/* Initializes the Listener */
		TcpListener ServerListener = new TcpListener(ipAd, PortNumber);
		TcpClient clientSocket = default(TcpClient);

		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			Thread ThreadingServer = new Thread(StartServer);
			ThreadingServer.Start();
		}


		private void THREAD_MOD(string teste)
		{
			textBox1.Text = teste;
			richTextBox1.Text += System.Environment.NewLine + teste;
		}

		private void StartServer()
		{
			String domain = "METROBILBAO";// maybe lowcare
			String username = "evacuate";
			String password = "Metro.2017";

			Action<string> DelegateTeste_ModifyText = THREAD_MOD;
			ServerListener.Start();
			Invoke(DelegateTeste_ModifyText, "Server waiting connections!");
			clientSocket = ServerListener.AcceptTcpClient();
			Invoke(DelegateTeste_ModifyText, "Server ready!");

			while (true)
			{
				try
				{

					NetworkStream networkStream = clientSocket.GetStream();
					byte[] bytesFrom = new byte[20];
					networkStream.Read(bytesFrom, 0, 20);
					string dataFromClient = System.Text.Encoding.ASCII.GetString(bytesFrom);
					dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("$"));

					string serverResponse = " -> ";

					if (dataFromClient.Equals("open"))
					{
						richTextBox1.Text += System.Environment.NewLine + "Command Received OPEN";
						serverResponse = " -> Open <- {Received!}";
						Citius.Evacuate.Clients.EvacuateNetTcpClient client = new Citius.Evacuate.Clients.EvacuateNetTcpClient(10);
						var cc = client.ClientCredentials.Windows.ClientCredential;
						cc.UserName = username;
						cc.Domain = domain;
						cc.Password = password;

						try
						{
							var b = client.AbrirPasos(-1);
							//MessageBox.Show(this, b.ToString());



							richTextBox1.Text += System.Environment.NewLine + b.ToString();
						}
						catch (Exception exc)
						{
							var _exc = exc;
						}
					}

					if (dataFromClient.Equals("close"))
					{
						richTextBox1.Text += System.Environment.NewLine + "Command Received CLOSE";
						serverResponse = " -> Close <- {Received!}";
						Citius.Evacuate.Clients.EvacuateNetTcpClient client = new Citius.Evacuate.Clients.EvacuateNetTcpClient(10);
						var cc = client.ClientCredentials.Windows.ClientCredential;
						cc.UserName = username;
						cc.Domain = domain;
						cc.Password = password;
						try
						{
							var b = client.CerrarPasos(-1);
							//MessageBox.Show(this, b.ToString());
							richTextBox1.Text += System.Environment.NewLine + b.ToString();
						}
						catch (Exception exc)
						{
							var _exc = exc;
						}
					}

					if (dataFromClient.Equals("recover"))
					{
						richTextBox1.Text += System.Environment.NewLine + "Command Received RECOVER";
						serverResponse = " -> Recover <- {Received!}";
						Citius.Evacuate.Clients.EvacuateNetTcpClient client = new Citius.Evacuate.Clients.EvacuateNetTcpClient(10);
						var cc = client.ClientCredentials.Windows.ClientCredential;
						cc.UserName = username;
						cc.Domain = domain;
						cc.Password = password;

						try
						{
							var b = client.ReponerEstados(-1);
							//MessageBox.Show(this, b.ToString());
							richTextBox1.Text += System.Environment.NewLine + b.ToString();
						}
						catch (Exception exc)
						{
							var _exc = exc;
						}
					}

					
					Byte[] sendBytes = Encoding.ASCII.GetBytes(serverResponse);
					networkStream.Write(sendBytes, 0, sendBytes.Length);
					networkStream.Flush();
				}
				catch
				{
					ServerListener.Stop();
					ServerListener.Start();
					Invoke(DelegateTeste_ModifyText, "Server waiting connections!");
					clientSocket = ServerListener.AcceptTcpClient();
					Invoke(DelegateTeste_ModifyText, "Server ready!");
				}

			}
		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{

		}
	}
}
