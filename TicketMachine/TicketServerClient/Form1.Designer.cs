﻿namespace TicketServerClient
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtStatus = new System.Windows.Forms.TextBox();
			this.txtEnviar = new System.Windows.Forms.TextBox();
			this.btnEnviar = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// txtStatus
			// 
			this.txtStatus.Location = new System.Drawing.Point(54, 45);
			this.txtStatus.Name = "txtStatus";
			this.txtStatus.Size = new System.Drawing.Size(468, 22);
			this.txtStatus.TabIndex = 0;
			// 
			// txtEnviar
			// 
			this.txtEnviar.Location = new System.Drawing.Point(54, 84);
			this.txtEnviar.Name = "txtEnviar";
			this.txtEnviar.Size = new System.Drawing.Size(468, 22);
			this.txtEnviar.TabIndex = 1;
			// 
			// btnEnviar
			// 
			this.btnEnviar.Location = new System.Drawing.Point(54, 129);
			this.btnEnviar.Name = "btnEnviar";
			this.btnEnviar.Size = new System.Drawing.Size(75, 23);
			this.btnEnviar.TabIndex = 2;
			this.btnEnviar.Text = "button1";
			this.btnEnviar.UseVisualStyleBackColor = true;
			this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(794, 440);
			this.Controls.Add(this.btnEnviar);
			this.Controls.Add(this.txtEnviar);
			this.Controls.Add(this.txtStatus);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load_1);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox txtStatus;
		private System.Windows.Forms.TextBox txtEnviar;
		private System.Windows.Forms.Button btnEnviar;
	}
}

