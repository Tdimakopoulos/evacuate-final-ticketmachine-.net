﻿namespace TicketMachine
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Lock = new System.Windows.Forms.Button();
			this.Unlock = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// Lock
			// 
			this.Lock.Location = new System.Drawing.Point(12, 21);
			this.Lock.Name = "Lock";
			this.Lock.Size = new System.Drawing.Size(124, 39);
			this.Lock.TabIndex = 0;
			this.Lock.Text = "Lock";
			this.Lock.UseVisualStyleBackColor = true;
			this.Lock.Click += new System.EventHandler(this.Lock_Click);
			// 
			// Unlock
			// 
			this.Unlock.Location = new System.Drawing.Point(12, 66);
			this.Unlock.Name = "Unlock";
			this.Unlock.Size = new System.Drawing.Size(124, 35);
			this.Unlock.TabIndex = 1;
			this.Unlock.Text = "Unlock";
			this.Unlock.UseVisualStyleBackColor = true;
			this.Unlock.Click += new System.EventHandler(this.Unlock_Click);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(156, 21);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(124, 39);
			this.button1.TabIndex = 2;
			this.button1.Text = "Lock 1";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(307, 21);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(124, 39);
			this.button2.TabIndex = 3;
			this.button2.Text = "Lock 2";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(156, 66);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(124, 35);
			this.button3.TabIndex = 4;
			this.button3.Text = "Unlock 1";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(307, 66);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(124, 35);
			this.button4.TabIndex = 5;
			this.button4.Text = "Unlock 2";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(447, 117);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.Unlock);
			this.Controls.Add(this.Lock);
			this.Name = "Form1";
			this.Text = "TicketMachine";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button Lock;
		private System.Windows.Forms.Button Unlock;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
	}
}

